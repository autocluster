#!/usr/bin/env python3

'''Print Samba version when run from top level of Samba git repository'''

import sys
sys.path.insert(0, './buildtools/wafsamba')
sys.path.insert(0, './third_party/waf')
import samba_version
import samba_waf18


class ENV:
    def __init__(self):
        self.env = {}
        self.env['GIT'] = '/usr/bin/git'
        self.GIT_LOCAL_CHANGES = None

    def get_flat(self, var):
        return self.env[var]

    def __iter__(self):
        return iter(self.env)


env = ENV()
v = samba_version.samba_version_file('VERSION', '.', env)
print(v.STRING)
